package main

import (
	"encoding/json"
	"log"

	"github.com/hashicorp/terraform-plugin-sdk/plugin"
	vtgrouper "github.com/terraform-provider-vt-grouper-group/vtgrouper"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: vtgrouper.Provider,
	})

	//makeRequest()
}

func makeRequest() {
	client := vtgrouper.NewGrouperClient("https://grouper.it.vt.edu/grouper-ws/servicesRest/json/v2_6_000", "myjwtuser", "/path/to/myjwtkey")
	data, err := client.Get("/groups/research.summit.app.irwin-test?with=all")

	if err != nil {
		log.Fatalln(err)
	}

	pretty, _ := json.MarshalIndent(data.Raw, "", "  ")
	log.Println(string(pretty))
}
