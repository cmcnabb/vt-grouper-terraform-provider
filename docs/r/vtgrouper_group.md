
# vtgrouper_folder

This resource allows you to create and manage folders within Grouper.

## Example usage

```hcl
resource "vtgrouper_folder" "my_folder" {
  name           = "dpt:nis:platform
}
```

## Argument Reference

The following arguments are supported:

- `name` - (Required) The name of the folder.

## Importing folders

You can import a folder using the `terraform import <resource> <id>` command, using the folder's name as it's `id` value. Example:

```bash
terraform import vtgrouper_folder.my_folder dpt:nis:platform
```

# vtgrouper_group

This resource allows you to create and manage a Grouper group.

## Example usage

```hcl
resource "vtgrouper_group" "my_group" {
  folder           = "dpt:nis:platform
  name             = "test1"
  members          = ["mikesir", "dpt:nis:platform:admins"]
}
```

## Argument Reference

The following arguments are supported:

- `folder` - (Required) The name of the folder containing the group.
- `name` - (Required) The name of the group.
- `members` - (Optional) A map of the `people` (PIDs) and `groups` that are members of this group

## Importing groups

You can import a group using the `terraform import <resource> <id>` command, using the group's folder and name as it's `id` value. Example:

```bash
terraform import vtgrouper_group.my_group dpt:nis:platform:test1
```

# Example

A slightly more complete usage example:

```
resource "vtgrouper_folder" "dpt-es" {
  name = "dpt:es"
}

resource "vtgrouper_folder" "dpt-es-org" {
  name = "${vtgrouper_folder.dpt-es.name}:org"
}

resource "vtgrouper_folder" "dpt-es-org-tf-testing" {
  name = "${vtgrouper_folder.dpt-es-org.name}:tf-testing"
}

resource "vtgrouper_group" "dpt-es-org-tf-testing-test-group" {
  folder = vtgrouper_folder.dpt-es-org-tf-testing.name
  name   = "tf-testing"
  members = ["akers", "jaylor"]
}
```